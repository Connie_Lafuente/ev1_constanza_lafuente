<%-- 
    Document   : index
    Created on : 03-oct-2020, 19:53:47
    Author     : Constance
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Aplicaciones Empresariales</title>
    </head>
    <body>
        <h1>Evaluación 1</h1>
        <h3>Ingrese su capital</h3>
        <form name="form" action="ControllerCalculator" method="POST">
            <div class="input-1">
                <label for="num1">Capital: </label>
                <input name="num1" class="form-control">
            </div>
            <br>
            <div class="input-2">
                <label for="num2">Tasa de Interés: </label>
                <input name="num2" class="form-control">
            </div>
            <br>
               <div class="input-3">
                <label for="num3">Años: </label>
                <input name="num3" class="form-control">
            </div>
            <br>
            <button type="submit" name="action" value="calculate" class="btn">Calcular</button>
             
        </form>
    </body>
</html>
